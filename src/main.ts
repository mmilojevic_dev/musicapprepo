// vue essentials
import Vue from "vue";
import { VueConstructor } from "vue";
import store from "./store";
import router from "./router";

// 3rd party libs
import vuetify from "./plugins/vuetify";

// components
import App from "./App.vue";
import Home from "./views/Home.vue";
import ChartsCard from "./components/ChartsCard.vue";
import Charts from "./components/Charts.vue";
import ChartContent from "./components/ChartContent.vue";
import ChartContentItem from "./components/ChartContentItem.vue";

// miscellaneous
import { convertToSnakeCase } from "./utils/helpers";

const components: { [name: string]: VueConstructor } = {
  App,
  Home,
  ChartsCard,
  Charts,
  ChartContent,
  ChartContentItem,
};

// automatically register all components
Object.keys(components).forEach((name: string) => {
  Vue.component(convertToSnakeCase(name), components[name]);
});

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
