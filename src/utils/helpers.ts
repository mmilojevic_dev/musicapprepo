/**
 * Convert to snake case
 *
 * @param {String} original - The original string
 * @returns {String}
 */
export function convertToSnakeCase(original: string): string {
  return original
    .replace(/\.?([A-Z]+)/g, (x: any, y: any): any => {
      return "-" + y.toLowerCase();
    })
    .replace(/^-/, "");
}

export default {
  convertToSnakeCase,
};
