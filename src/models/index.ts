export class IChart {
  id = 0;
  name = ChartEnum.Artists;
  endpoint = ChartEndpointEnum.GetArtists;
  getter = ChartGetterEnum.ArtistsGetter;
  icon = ChartIconEnum.ArtistIcon;
}

export class IChartItem {
  params: any = {};
  endpoint = "";
  getter = "";
  group = "";
}

export interface IArtist {
  name: string;
  playcount: string;
  listeners: string;
  mbid: string;
  url: string;
  streamable: string;
  image: any[];
}

export interface ITag {
  name: string;
  url: string;
  reach: string;
  taggings: string;
  streamable: string;
  wiki: any;
}

export interface ITrack {
  name: string;
  duration: string;
  playcount: string;
  listeners: string;
  mbid: string;
  url: string;
  streamable: any;
  artist: any;
  image: any[];
}

export enum ChartEnum {
  Artists = "artists",
  Tracks = "tracks",
  Tags = "tags",
}

export enum ChartEndpointEnum {
  GetArtists = "getTopArtists",
  GetTracks = "getTopTracks",
  GetTags = "getTopTags",
}

export enum ChartGetterEnum {
  ArtistsGetter = "artistsGetter",
  TracksGetter = "tracksGetter",
  TagsGetter = "tagsGetter",
}

export enum ChartItemEndpointEnum {
  GetArtistInfo = "getInfo",
  GetTrackInfo = "getInfo",
  GetTagInfo = "getInfo",
}

export enum ChartItemGetterEnum {
  ArtistInfoGetter = "artistInfoGetter",
  TrackInfoGetter = "trackInfoGetter",
  TagInfoGetter = "tagInfoGetter",
}

export enum ChartIconEnum {
  ArtistIcon = "mdi-account-music",
  TrackIcon = "mdi-music",
  TagIcon = "mdi-tag",
}
