// import dependency to handle HTTP request to our back end
import {
  ChartEndpointEnum,
  ChartEnum,
  ChartGetterEnum,
  ChartIconEnum,
  IArtist,
  IChart,
  IChartItem,
  ITag,
  ITrack,
} from "@/models";
import constants from "@/utils/constants";
import axios, { AxiosResponse } from "axios";
import Vue from "vue";
import Vuex from "vuex";
const { API } = constants;

//load Vuex
Vue.use(Vuex);

// *** HANDLE NON-CACHED (STATIC) STATE ***
const state = {
  charts: [
    {
      id: 0,
      name: ChartEnum.Artists,
      endpoint: ChartEndpointEnum.GetArtists,
      getter: ChartGetterEnum.ArtistsGetter,
      icon: ChartIconEnum.ArtistIcon,
    },
    {
      id: 1,
      name: ChartEnum.Tracks,
      endpoint: ChartEndpointEnum.GetTracks,
      getter: ChartGetterEnum.TracksGetter,
      icon: ChartIconEnum.TrackIcon,
    },
    {
      id: 2,
      name: ChartEnum.Tags,
      endpoint: ChartEndpointEnum.GetTags,
      getter: ChartGetterEnum.TagsGetter,
      icon: ChartIconEnum.TagIcon,
    },
  ],
  chart: new IChart(),
  chartItems: [
    new IChartItem(),
    new IChartItem(),
    new IChartItem(),
    new IChartItem(),
    new IChartItem(),
  ],
  chartItem: new IChartItem(),
  artists: [],
  tracks: [],
  tags: [],
  itemInfo: <any>{},
  chartItemInfoState: false,
};

// *** HANDLE CACHED STATE THROUGH GETTERS ***
const getters = {
  chartsGetter: (state: { charts: IChart[] }) => state.charts,
  chartGetter: (state: { chart: IChart }) => state.chart,
  chartItemsGetter: (state: { chartItems: IChartItem[] }) => state.chartItems,
  chartItemGetter: (state: { chartItem: IChartItem }) => state.chartItem,
  artistsGetter: (state: { artists: IArtist[] }) => state.artists,
  tracksGetter: (state: { tracks: ITrack[] }) => state.tracks,
  tagsGetter: (state: { tags: ITag[] }) => state.tags,
  itemInfoGetter: (state: { itemInfo: any }) => state.itemInfo,
  chartItemInfoStateGetter: (state: { chartItemInfoState: boolean }) =>
    state.chartItemInfoState,
};

// *** HANDLE ACTIONS ***
const actions = {
  // select chart to active state and to render the corresponding list from the api
  selectChart({ commit }: any, payload: IChart) {
    commit("SET_CHART", payload);
  },

  // select chart item in order to view the corresponding additional info
  selectChartItem({ commit }: any, payload: IChartItem) {
    commit("SET_CHART_ITEM", payload);
  },

  // get top artist
  getTopArtists({ commit }: any) {
    const endpoint = `${API.GROUPCHART}.${ChartEndpointEnum.GetArtists}`;
    axios
      .get(
        `${API.HOST}?method=${endpoint}&api_key=${API.KEY}&format=json&limit=${API.LIMIT}`
      )
      .then((response: AxiosResponse) => {
        commit("SET_ARTISTS", response.data.artists.artist);
      });
  },

  // get top tracks
  getTopTracks({ commit }: any) {
    const endpoint = `${API.GROUPCHART}.${ChartEndpointEnum.GetTracks}`;
    axios
      .get(
        `${API.HOST}?method=${endpoint}&api_key=${API.KEY}&format=json&limit=${API.LIMIT}`
      )
      .then((response: AxiosResponse) => {
        commit("SET_TRACKS", response.data.tracks.track);
      });
  },

  // get top tags
  getTopTags({ commit }: any) {
    const endpoint = `${API.GROUPCHART}.${ChartEndpointEnum.GetTags}`;
    axios
      .get(
        `${API.HOST}?method=${endpoint}&api_key=${API.KEY}&format=json&limit=${API.LIMIT}`
      )
      .then((response: AxiosResponse) => {
        commit("SET_TAGS", response.data.tags.tag);
      });
  },

  // get item info
  getItemInfo({ commit }: any, payload: any) {
    const endpoint = `${payload.group}.getInfo`;
    axios
      .get(API.HOST, {
        params: {
          ...payload.params,
          method: endpoint,
          api_key: API.KEY,
          format: "json",
        },
      })
      .then((response: AxiosResponse) => {
        const data = response.data[payload.group];
        if (data) {
          data.chart = payload.group;
        }
        commit(`SET_ITEM_INFO`, data);
      });
  },

  // handle the visibility state of the info card
  setChartItemInfoState({ commit }: any, payload: boolean) {
    commit("SET_CHART_ITEM_INFO_STATE", payload);
  },
};

// *** HANDLE MUTATIONS ***
const mutations = {
  // mutate chart
  SET_CHART(state: { chart: IChart }, chart: IChart) {
    state.chart = chart;
  },

  // mutate chart
  SET_CHART_ITEM(state: { chartItem: IChartItem }, chartItem: IChartItem) {
    state.chartItem = chartItem;
  },

  // mutate artists
  SET_ARTISTS(state: { artists: IArtist[] }, artists: IArtist[]) {
    state.artists = artists;
  },

  // mutate tracks
  SET_TRACKS(state: { tracks: ITrack[] }, tracks: ITrack[]) {
    state.tracks = tracks;
  },

  // mutate tags
  SET_TAGS(state: { tags: ITag[] }, tags: ITag[]) {
    state.tags = tags;
  },

  // mutate item info
  SET_ITEM_INFO(state: { itemInfo: any }, itemInfo: any) {
    state.itemInfo = itemInfo;
  },

  // mutate chart item info state
  SET_CHART_ITEM_INFO_STATE(
    state: { chartItemInfoState: boolean },
    chartItemInfoState: boolean
  ) {
    state.chartItemInfoState = chartItemInfoState;
  },
};

//export store module
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
});
