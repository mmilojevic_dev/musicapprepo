# music-app

This is a mini-app witch fetches top artists/tracks/tags charts from the last.fm API and displaying the data through the lists, switching them by clickable tabs, and showing the additional info for each chart item when clicked on it by rendering the new card.

Core component is the App component. Although, it extends Base component which can be found in the /src folder. I left it there, only for purpose to wrap the App component, and escape the usage of the constructor in the every class, and if you take a look in the main.ts file, I used VueConstructor and registered the components. This approach was recreated from my last and only Vue project.

Components can be found in the src/components folder, named in a way to correspond to the visual presentation and the data that they consume.

There is also a single component in src/view, "Home", since we do not have any real routing.

State management was handled with VueX, all state-related code can be found in the src/store/index.ts.

In the src/utils you can find constants that I used mainly for the API related actions.

Initial plan was to move the API requests to the dedicated place src/api/index.ts, but remained as a todo.

## Project setup

```
instal node
```

```
run npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

```
open browser http://localhost:8080/
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
